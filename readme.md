# Setup

See `requirement.txt` and `https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2.md` for packages install.

Resize your dataset with imagemagick :
    
    `sudo apt-get install imagemagick`

    `mogrify -resize 300x300! *.jpg` # don't keep image aspect ratio

# Dataset Generator

Please ensure `path` variable is an absolute path !

Dataset needs to be composed of .jpg files --> Datasets/Images folder

# Network Training

Please ensure `path` variable is an absolute path !

Notebook designed for training on GPU, you can modify it easily to adapt otherwise.

In last cell, you need to edit location of `model_main_tf2.py` according your build installation of tensorflow/models.